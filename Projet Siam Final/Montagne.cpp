#include "Montagnes.h"

using namespace std;

Montagnes::Montagnes(float _force, string _lettre): Pieces(_force, _lettre)
{
    //constructeur surchargé
}

Montagnes::Montagnes(const Montagnes & montagne): Pieces(montagne.force, montagne.lettre)
{
    //constructeur par copie
}




string Montagnes::symbole()
{
	return lettre;  //Retourne le symbole des montagnes a afficher sur le plateau
}

int Montagnes::typePiece() {
	return 3;
}


