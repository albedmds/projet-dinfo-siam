#ifndef _MONTAGNES_H_
#define _MONTAGNES_H_

#include <string>
#include "Pieces.h"

using namespace std;

class Montagnes : public Pieces //h�ritage
{


	public:
		Montagnes(float _force, std::string _lettre);       //constructeurs et m�thodes
		Montagnes(const Montagnes & montagne);
		virtual std::string symbole();
		virtual int typePiece();

};

#endif
