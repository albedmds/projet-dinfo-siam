#include "Pieces.h"

using namespace std;

Pieces::Pieces(float _force, string _lettre): force(_force), lettre(_lettre) //cosntructeur surchargé
{

}

Pieces::Pieces(const Pieces & piece): force(piece.force), lettre(piece.lettre) //constructeur par copie
{

}

float Pieces::getForce() const //accesseur
{
	return force;

}

Pieces::~Pieces()
{

}
