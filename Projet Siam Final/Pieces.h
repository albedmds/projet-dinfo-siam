#ifndef _PIECES_H_
#define _PIECES_H_

#include <string>


class Pieces
{

	protected:
		float force;            //attributs
		std::string lettre;

	public:
		Pieces(float _force, std::string _lettre);      //construceur et m�thodes
		Pieces(const Pieces & piece);
		virtual std::string symbole()=0;        //m�thodes virtuelles
		virtual int typePiece()=0;
		virtual ~Pieces();


		float getForce() const;  //accesseur

};

#endif
