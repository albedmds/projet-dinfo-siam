#include "PiecesJouables.h"


using namespace std;


PiecesJouables:: PiecesJouables(float _force, string _lettre, string _orientation): Pieces(_force, _lettre), orientation(_orientation)
{
    //construteur surchargé
}


string PiecesJouables::getOrientation() const
{
	return orientation;     //accesseur
}

void PiecesJouables::setOrientation(string _orientation)
{
	orientation = _orientation; //setter
}

string PiecesJouables::symbole()
{
	return lettre+orientation; //concatene les strings de la piece
}






