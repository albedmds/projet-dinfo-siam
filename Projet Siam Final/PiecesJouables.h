#ifndef _PIECESJOUABLES_H_
#define _PIECESJOUABLES_H_

#include <string>

#include "Pieces.h"
#include "console.h"

class PiecesJouables : public Pieces //h�ritage
{

	protected:
		std::string orientation;        //attributs


	public:
		PiecesJouables(float _force, std::string _lettre, std::string _orientation);        //m�thodes et constructeur

		std::string getOrientation() const;
		void setOrientation(std::string _orientation);
		virtual std::string symbole();

};

#endif
