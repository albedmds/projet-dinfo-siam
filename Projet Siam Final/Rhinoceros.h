#ifndef _RHINOCEROS_H_
#define _RHINOCEROS_H_

#include<string>

#include "PiecesJouables.h"

class Rhinoceros : public PiecesJouables //h�ritage
{


  public:
    Rhinoceros(float _force, std::string _lettre, std::string _orientation); //m�thode et constructeur

	virtual int typePiece();

};

#endif
