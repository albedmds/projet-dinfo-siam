#include "Siam.h"
#include "functions.h"

using namespace std;

/*
    Constructeur surcharg�
*/
Siam::Siam(const int & _nbPieces, const int &  _nbMontagne, const int &  _nbLignePlateau, const int &  _nbColPlateau, const float & _forcePiece, const float & _forcemontagne, const string & _lettreElephant, const string & _lettreRhinoceros, const string & _lettreMontagne, const string & _lettresOrientation)
	: nbPieces(_nbPieces), nbMontagne(_nbMontagne), forcePiece(_forcePiece), forcemontagne(_forcemontagne), lettreElephant(_lettreElephant), lettreRhinoceros(_lettreRhinoceros), lettreMontagne(_lettreMontagne), lettresOrientation(_lettresOrientation)
{
	plateau = new Plateau(_nbLignePlateau, _nbColPlateau);
}

/*
	Destructeur
*/
Siam::~Siam()
{
    delete plateau;
}


/*
	Verifie si la lettre utilis� pour indiquer l'orientation est valide
*/
bool Siam::validOrientation(const string & orientation) const {
	bool recherche = false;
	for(unsigned int i = 0 ; i < lettresOrientation.size(); i++){
		if(lettresOrientation.substr(i,1) == orientation) //on parcours les 4 lettres d'orientation au moyen de substr(pos,length)
        {                                                  //si il y a match on return true
			recherche = true;
		}
	}

	return recherche;
}

/*
	Joue une partie
*/
void Siam::jouer() {

	/* Cr�ation des Montagnes */
	vector<Montagnes *> tabMontagne;
	for(int i = 0; i<nbMontagne; i++)
    {
		tabMontagne.push_back(new Montagnes(forcemontagne , lettreMontagne));
	}

	/* les Montagnes sont plac�s sur le plateau */
	plateau->initialiseMontagnes(tabMontagne);
	plateau->dessiner();

	int i = 0;

	/*
		Tant que le nombre de montagne sur le plateau est superieur ou egal au nombre de montagne initiale la partie continue
	*/
	while(plateau->nombreMontagnes() >= nbMontagne)
        {

            if(i%2 == 0)
                {
                    cout << "  "<< "______________________________________" << "  " << endl;
                    cout << "| \t\t\t\t\t |" << endl;
                    cout << "| \t"<< "Elephants a vous de jouer !" << "\t |" << endl;
                    cout << "| "<< "______________________________________" << " |" << endl;
                    cout << endl;
                }
            else
                {
                    cout << "  "<< "______________________________________" << "  " << endl;
                    cout << "| \t\t\t\t\t |" << endl;
                    cout << "| \t"<< "Rhinoceros a vous de jouer !" << "\t |" << endl;
                    cout << "| "<< "______________________________________" << " |" << endl;
                    cout << endl;
                }

		bool validOption = false;
		while(validOption == false)
            {
                validOption = true;
                int option = this->menu(i);

                switch (option)
                {
                    case 1:
                        this->ajouterPiece(i);
                        break;

                    case 2:
                        this->deplacerPiece(i);
                        break;

                    case 3:
                        this->orienterPiece(i);
                        break;

                    case 4:
                        this->retirerPiece(i);
                        break;

                    case 5:
                        this->pousserPiece(i);
                        break;

                    default:
                        validOption = false;
                        break;
                }
            system("CLS");
			plateau->dessiner();
		}
		i++;
	}

	if(plateau->nombreMontagnes() < nbMontagne)
    {
        cout<<"FELICITATIONS valeureux ";   //message de victoire
        if(i%2 == 0)
                {
                    cout<<"Rhinoceros"<<endl;
                    cout<<"Ta victoire montre tes grandes qualites qui sont la ruse et le courage, tu as reussi a dominer le royaume de Siam"<<endl;
                }
            else
                {
                    cout<<"Elephant"<<endl;
                    cout<<"Ta victoire montre tes grandes qualites qui sont la ruse et le courage, tu as reussi a dominer le royaume de Siam"<<endl;
                }
    }

}

/*
	Affiche la liste des options aux joueurs
*/
int Siam::menu(int joueur) const{

	int nombrePiece = 0;
	if(joueur%2 == 0)       //differenciation des deux joueurs
    {
		nombrePiece = plateau->nombreElephants();
	}
	else
	{
		nombrePiece = plateau->nombreRhinoceros();
	}

	int option = 0;
	if(nombrePiece > 0) //si il n'y as pas de pieces, obliger d'en inserer une
        {
		cout << "Que Souhaitez vous faire ?" << endl;
		cout << "Tapez 1 pour" << "\t " << "Entrer un de vos animaux sur le plateau"   <<endl;
		cout << "Tapez 2 pour" << "\t " << "Vous deplacer sur une case libre" <<endl;
		cout << "Tapez 3 pour" << "\t " << "Changer l'orientation de votre animal" <<endl;
		cout << "Tapez 4 pour" << "\t " << "Sortir un de vos animaux du plateau" <<endl;
		cout << "Tapez 5 pour" << "\t " << "Pousser d'autres pieces disposees sur le plateau" <<endl;

		cin >> option;
	}
	else
    {
		option = 1;
	}

	return option;
}

/*

*/
void Siam::ajouterPiece(int joueur)
{
	bool validCoordonne = false;
	int col, ligne;
	string orientation;

	do {
		cout << "Ajouter l'une de vos pieces sur le plateau" << endl;
		cout << "Entrer les coordonnees et l'orientation (exemple 'A1b') : ";
		string coordonne = "";
		cin >> coordonne;

		if(coordonne.size()==3)
            {
                //col = stoi(coordonne.substr(1,1)) - 1; ne marche pas sur cod::blocks :(( et evitait ce pseudo-switch en 1 ligne
                if(coordonne.substr(1,1) == "1")        //on recupere le 2e caractere de la chaine et en fonction de �a valeur on l'affecte a une des colonnes
                    col = 0;
                if(coordonne.substr(1,1) == "2")
                    col = 1;
                if(coordonne.substr(1,1) == "3")
                    col = 2;
                if(coordonne.substr(1,1) == "4")
                    col = 3;
                if(coordonne.substr(1,1) == "5")
                    col = 4;

                ligne = plateau->getLibelleLigne().find(coordonne.substr(0,1)); //on recupere le 1er caractere de la chaine et on l'affecte la ligne correspondante
                orientation = coordonne.substr(2,1);

                if(plateau->caseValid(ligne, col) && plateau->bordure(ligne, col) && plateau->caseLibre(ligne, col) && validOrientation(orientation))
                    {
                    validCoordonne = true; //si toutes les conditions sont bonnes on effectue l'action
                    }
		}

	} while(!validCoordonne);


	Pieces * piece = 0;
	if(joueur%2 == 0)       //on affecte la bonne entit� en fonction du joueur
    {
		Elephants * E = new Elephants(forcePiece, lettreElephant, orientation);
		piece = E;
	}
	else {
		Rhinoceros * R = new Rhinoceros(forcePiece, lettreRhinoceros, orientation);
		piece = R;
	}

	//Ajouter la piece au plateau
	plateau->ajouterPiece(piece, ligne, col);
	plateau->typePiece(ligne, col);
}


void Siam::deplacerPiece(int joueur) {
	bool validCoordonne = false;
	int colPiece, lignePiece, colDestination, ligneDestination;

	do {
		cout << "Deplacer l'une de vos pieces sur le plateau" << endl;
		cout << "Entrer les coordonnees de la piece a deplacer : ";
		string coordonnePiece = "";
		cin >> coordonnePiece;

		if(coordonnePiece.size()==2) {
			//colPiece = stoi(coordonnePiece.substr(1,1)) - 1;
			if(coordonnePiece.substr(1,1) == "1")           //idem que pour ajouterPiece
				colPiece = 0;
			if(coordonnePiece.substr(1,1) == "2")
				colPiece = 1;
			if(coordonnePiece.substr(1,1) == "3")
				colPiece = 2;
			if(coordonnePiece.substr(1,1) == "4")
				colPiece = 3;
			if(coordonnePiece.substr(1,1) == "5")
				colPiece = 4;

			lignePiece = plateau->getLibelleLigne().find(coordonnePiece.substr(0,1));

			int typeJoueur = 0;
			if(joueur%2 == 0) {
				typeJoueur = 1;
			}
			else {
				typeJoueur = 2;
			}

			if(plateau->typePiece(lignePiece, colPiece) == typeJoueur) //si le joueur choisit une de ses entit�s
                {
				cout << "Entrer les coordonnees de destination de la piece : ";
				string coordonneDestination = "";
				cin >> coordonneDestination;

				if(coordonneDestination.size()==2)
                    {
					//colDestination = stoi(coordonneDestination.substr(1,1)) - 1;
					if(coordonneDestination.substr(1,1) == "1")
						colDestination = 0;
					if(coordonneDestination.substr(1,1) == "2")
						colDestination = 1;
					if(coordonneDestination.substr(1,1) == "3")
						colDestination = 2;
					if(coordonneDestination.substr(1,1) == "4")
						colDestination = 3;
					if(coordonneDestination.substr(1,1) == "5")
						colDestination = 4;
					ligneDestination = plateau->getLibelleLigne().find(coordonneDestination.substr(0,1));
					//
					if(plateau->caseValid(ligneDestination, colDestination) && plateau->caseLibre(ligneDestination, colDestination) && plateau->caseAdjacente(lignePiece, colPiece, ligneDestination, colDestination) ) {
						validCoordonne = true;
					}
				}
			}
		}

	}while(!validCoordonne);

	//Deplacer la piece au plateau
	plateau->deplacerPiece(lignePiece, colPiece, ligneDestination, colDestination); //alors on change sa position

}

void Siam::retirerPiece(int joueur)
{

    bool validCoordonne = false;
	int col, ligne;

	while(!validCoordonne)
	 {
		cout << "Retirer l'une de vos pieces sur le plateau" << endl;
        cout << "Entrer les coordonnees : ";
        string coordonne = "";
        cin >> coordonne;

		if(coordonne.size()==2) {
			//col = stoi(coordonne.substr(1,1)) - 1;
			if(coordonne.substr(1,1) == "1")            //idem
				col = 0;
			if(coordonne.substr(1,1) == "2")
				col = 1;
			if(coordonne.substr(1,1) == "3")
				col = 2;
			if(coordonne.substr(1,1) == "4")
				col = 3;
			if(coordonne.substr(1,1) == "5")
				col = 4;

			ligne = plateau->getLibelleLigne().find(coordonne.substr(0,1));


			if(plateau->caseValid(ligne, col) && plateau->bordure(ligne, col)) //si la case existe et est en bordure
			{
				if(joueur%2 == 0)
				{
					if(plateau->typePiece(ligne, col)==1)      //si joueur elephants ok
					{
						plateau->supprimerPiece(ligne,col);
						validCoordonne = true;
					}
				}

				if(joueur%2 != 0)
				{
					if(plateau->typePiece(ligne, col)==2)      //si joueur rhino ok
					{
						plateau->supprimerPiece(ligne,col);
						validCoordonne = true;
					}
				}
            }
		}
    }
}

void Siam::orienterPiece(int joueur)
{
	bool validCoordonne = false;
	int col, ligne;
	string orientation;

	do {
		cout << "Modifier l'orientation de l'une de vos pieces" << endl;
		cout << "Entrer les coordonnees de la piece � orienter : ";
		string coordonne = "";
		cin >> coordonne;

		if(coordonne.size()==2) {
			//col = stoi(coordonne.substr(1,1)) - 1;
			if(coordonne.substr(1,1) == "1")
				col = 0;
			if(coordonne.substr(1,1) == "2")            //idem
				col = 1;
			if(coordonne.substr(1,1) == "3")
				col = 2;
			if(coordonne.substr(1,1) == "4")
				col = 3;
			if(coordonne.substr(1,1) == "5")
				col = 4;

			ligne = plateau->getLibelleLigne().find(coordonne.substr(0,1));

			int typeJoueur = 0;
			if(joueur%2 == 0)
                {
				typeJoueur = 1;
			    }
			else {
				typeJoueur = 2;
			}

			if(plateau->typePiece(ligne, col) == typeJoueur) //si l'entit� match le joueur
                {

				cout << "Entrer l'orientation de la piece : ";
				string orientation = "";                            //on saisit la nouvelle orientation
				cin >> orientation;

				if(orientation.size()==1)
                    {
					if(this->validOrientation(orientation)) //si elle est valide on l'affecte
					{
						validCoordonne = true;
						//orienter la piece
						plateau->orienterPiece(ligne, col, orientation);
					}
				}
			}
		}

	} while(!validCoordonne);
}

/*

*/
void Siam::pousserPiece(int joueur) {

	bool validCoordonne = false;
	int colPiece, lignePiece;

	do {
		cout << "Pousser l'une de vos pieces sur le plateau" << endl;
		cout << "Entrer les coordonnees de la piece a deplacer : ";
		string coordonnePiece = "";
		cin >> coordonnePiece;

		if(coordonnePiece.size()==2) {
			//colPiece = stoi(coordonnePiece.substr(1,1)) - 1;  ==> marche pas sur cod::blocks
			if(coordonnePiece.substr(1,1) == "1")
				colPiece = 0;
			if(coordonnePiece.substr(1,1) == "2")       //idem
				colPiece = 1;
			if(coordonnePiece.substr(1,1) == "3")
				colPiece = 2;
			if(coordonnePiece.substr(1,1) == "4")
				colPiece = 3;
			if(coordonnePiece.substr(1,1) == "5")
				colPiece = 4;

			lignePiece = plateau->getLibelleLigne().find(coordonnePiece.substr(0,1));

			int typeJoueur = 0;
			if(joueur%2 == 0)
                {
				typeJoueur = 1;
			    }
			else {
				typeJoueur = 2;
			}

			if(plateau->typePiece(lignePiece, colPiece) == typeJoueur) //si le joueur match son entit�
                {
				vector<Pieces *> listePiece = plateau->piecesAligner(lignePiece, colPiece); //on rempli un vecteur avec toutes les entit�s sur la ligne ou la colonne

				if(listePiece.size() >1 )
                    {
					string orientationPiece = "";       //variables
					string orientationContraire = "";
					float force = 0.0;

					try {
						PiecesJouables * pieceJouable = dynamic_cast<PiecesJouables*>(listePiece[0]); //cast de la  1ere case de la liste qui contient la piece selectionn�e
						orientationPiece = pieceJouable->getOrientation();//acces a l'orientation et la force
						force = pieceJouable->getForce();
					}
					catch (const exception& e)
					{
						cerr << e.what(); //message d'erreur si �a ne marche pas
					}

					if(orientationPiece == "h")     //d�claration des cases qui importes
						orientationContraire = "b";
					if(orientationPiece == "d")
						orientationContraire = "g";
					if(orientationPiece == "b")
						orientationContraire = "h";
					if(orientationPiece == "g")
						orientationContraire = "d";

					for(unsigned int i = 1; i<listePiece.size(); i++)
                        {
						string orientationPiecePousser = "";        //d�cla variables
						float forcePiecePousser = listePiece[i]->getForce();
						int typePiece = listePiece[i]->typePiece();

						// Si ce n'est pas une Montagne
						if(typePiece != 3) {
							try {
								PiecesJouables * pieceJouable = dynamic_cast<PiecesJouables*>(listePiece[i]); //cast et get de l'orientation de toutes les autres pieces
								orientationPiecePousser = pieceJouable->getOrientation();
							}
							catch (const exception& e)
							{
								cerr << e.what(); //message d'erreur si ne fonctionne pas
							}
						}

						if(orientationPiecePousser == orientationContraire)
                            {
							force -= forcePiecePousser;//on d�cr�mente la force de la piece selectionn�e en fonctions des resistances des autres pieces
						    }
						else if(orientationPiecePousser == orientationPiece && typePiece == typeJoueur ){
							force += forcePiecePousser;//on incremente la force de la piece selectionn�e si elle est dans le m�me sens
						}
					}

					if(force > 0) {
						plateau->pousserPiece(lignePiece, colPiece); //si la pouss�e est possible,on pousse
						validCoordonne = true;
					}
				}
			}
		}
	}while(!validCoordonne);

}










