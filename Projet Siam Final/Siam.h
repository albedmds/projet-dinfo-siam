#ifndef _SIAM_H_
#define _SIAM_H_

#include <vector>

#include "Elephants.h"
#include "Rhinoceros.h"
#include "Montagnes.h"
#include "Plateau.h"

using namespace std;

class Siam {

	private:
		int nbPieces;               //attributs
		int nbMontagne;
		float forcePiece;
		float forcemontagne;
		std::string lettreElephant;
		std::string lettreRhinoceros;
		std::string lettreMontagne;
		std::string lettresOrientation;

		Plateau *plateau;

	public:
		Siam(const int & _nbPieces, const int &  _nbMontagne, const int &  _nbLignePlateau, const int &  _nbColPlateau, const float & _forcePiece, const float & _forcemontagne, const std::string & _lettreElephant, const std::string & _lettreRhinoceros, const std::string & _lettreMontagne, const std::string & _lettresOrientation);
		~Siam();                //constructeur et destructeur

		void jouer();           //methodes
		bool validOrientation(const std::string & orientation) const;
		int menu(int joueur) const;
		void ajouterPiece(int joueur);
		void deplacerPiece(int joueur);
		void retirerPiece(int joueur);
		void orienterPiece(int joueur);
		void pousserPiece(int joueur);
};

#endif
