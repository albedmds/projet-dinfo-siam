#ifndef ANIMAL_H_INCLUDED
#define ANIMAL_H_INCLUDED

class Animal : public Piece
{
private:
    int m_orientation;

public:
    Animal();
    ~Animal();

    void tourner();
    void pousser();
};

#endif // ANIMAL_H_INCLUDED
