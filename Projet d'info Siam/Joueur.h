#ifndef JOUEUR_H_INCLUDED
#define JOUEUR_H_INCLUDED

class Joueur
{
private:
    std::string m_nom;
    bool m_tour_jeu;
    int m_espece;

public:
    Joueur();
    ~Joueur();
};
#endif // JOUEUR_H_INCLUDED
