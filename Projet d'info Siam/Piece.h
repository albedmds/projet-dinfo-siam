#ifndef PIECE_H_INCLUDED
#define PIECE_H_INCLUDED

class Piece
{
private:
    int m_abcisse;
    int m_ordonnee;
    float m_poids;
    bool m_actif;
    int m_espece;

public:
    Piece();
    virtual ~Piece();

    float getPoids()
    {
        return m_poids;
    }
    void setPoids(float _poids)
    {
        m_poids=_poids;
    }

    int getEspece()
    {
        return m_espece;
    }
    void setEspece(int _espece)
    {
        m_espece=_espece;
    }

    void deplacer();
    void activation();

};

#endif // PIECE_H_INCLUDED
