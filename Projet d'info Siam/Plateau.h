#ifndef PLATEAU_H_INCLUDED
#define PLATEAU_H_INCLUDED

class Plateau
{
private:
    std::vector<std::vector<Piece*> > m_tab;

public:
    Plateau();
    virtual ~Plateau();
    void afficher();

};

#endif // PLATEAU_H_INCLUDED
