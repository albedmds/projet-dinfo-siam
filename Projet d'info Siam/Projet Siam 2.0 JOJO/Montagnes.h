#ifndef _MONTAGNES_H_
#define _MONTAGNES_H_

#include <string>
#include "Pieces.h"

using namespace std;

class Montagnes : public Pieces {


	public:
		Montagnes(float _force, std::string _lettre);
		Montagnes(const Montagnes & montagne);
		virtual std::string symbole();
		virtual int typePiece();
};

#endif