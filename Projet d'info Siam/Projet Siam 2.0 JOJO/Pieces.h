#ifndef _PIECES_H_
#define _PIECES_H_

#include <string>


class Pieces {

	protected:
		float force;
		std::string lettre;

	public:
		Pieces(float _force, std::string _lettre);
		Pieces(const Pieces & piece);
		virtual std::string symbole()=0;
		virtual int typePiece()=0;
};

#endif