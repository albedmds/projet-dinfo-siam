#include "PiecesJouables.h"

using namespace std;


PiecesJouables:: PiecesJouables(float _force, string _lettre, string _orientation): Pieces(_force, _lettre), orientation(_orientation) {
}


string PiecesJouables::getOrientation() {
	return orientation;
}

void PiecesJouables::setOrientation(string _orientation) {
	orientation = _orientation;
}

string PiecesJouables::symbole() {
	return lettre+orientation;
}