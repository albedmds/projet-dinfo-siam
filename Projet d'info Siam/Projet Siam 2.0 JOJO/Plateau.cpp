#include "Plateau.h"

using namespace std;

/*
	Constructeur, initialise le plateau
*/
Plateau::Plateau(int _nbLigne, int _nbCol):nbLigne(_nbLigne), nbCol(_nbCol), libelleLigne("ABCDE") {
	for(int i = 0; i<nbLigne; i++) {
		grille.push_back(vector<Pieces *>(nbCol, nullptr));
	}
}

/*
	Ajoute un pointeur de "Pieces" au plateau
*/
void Plateau::ajouterPiece(Pieces * p, const int & ligne, const int & col) {
	grille[ligne][col] = p;
}

/*
	Supprime un pointeur de "Pieces" du plateau
*/
void Plateau::supprimerPiece(const int & ligne, const int & col) {
	grille[ligne][col] = 0;
}

/*
	Deplace un pointeur de "Pieces" du plateau
*/
void Plateau::deplacerPiece(const int & lignePiece, const int & colPiece, const int & ligneDestination, const int & colDestination){
	this->ajouterPiece(grille[lignePiece][colPiece], ligneDestination, colDestination);
	this->supprimerPiece(lignePiece, colPiece);
}


/*
	Place les montagnes a leurs positions initiales (au centre du plateau)
*/
void Plateau::initialiseMontagnes(const vector<Montagnes *> & tabMontagne) {
	int tabSize = tabMontagne.size();
	for(int i = 0; i<tabSize; i++) {
		grille[i+1][2] = tabMontagne[i];
	}
}

/*
	Affiche le plateau dans la console
*/
void Plateau::dessiner() {
	cout << endl << endl;

	for(int i = 1; i<=nbCol; i++) {
		cout << "\t" << i ;
	}
	cout << endl;
	for(int i = 1; i<=nbCol; i++) {
		cout << "\t_";
	}

	cout << endl << endl;

	for(int i = 0; i<nbLigne; i++) {
		cout << " " << libelleLigne[i]<<" |";
		for(int j = 0; j<nbCol; j++) {

			if(grille[i][j] != 0) {
				cout << "\t" << grille[i][j]->symbole();
			}
			else {
				cout << "\t" << "X";
			}
		}
		cout << endl << endl;
	}

}

/*

*/
string Plateau::getLibelleLigne() {
	return libelleLigne;
}

/*
	Retourne true si les coordonnees sont valides
*/
bool Plateau::caseValid(const int & i, const int & j) {
	if(i>=0 && i<nbLigne && j>=0 && j<nbCol)
        return true;
    else return false;
}

/*
	Retourne true si les coordonnees correspondent a une case vide du plateau
*/
bool Plateau::caseLibre(const int &  i, const int &  j) {
    if(grille[i][j] == 0)
        return true;
    else return false;
}

/*
	Retourne true si les coordonnees sont au bord du plateau
*/
bool Plateau::bordure(const int & i, const int & j) {
	if((i == 0 || i == 4 || j == 0 || j == 4))
        return true;
    else return false;
}

/*
	Retourne true si les coordonnes de la case (i,j) et adjacente a la case (k,l)
*/
bool Plateau::caseAdjacente(const int & i, const int & j, const int & k, const int & l) {
	if((i == k && (j==l+1 || j==l-1)) || (j==l && (i==k+1 || i==k-1) ))
		return true;
    else return false;
}

/*
	Identifie le type de piece sur une case
	Retourne 1 pour les Elephants, 2 pour les Rhino, 3 pour les Montagnes
*/
int Plateau::typePiece(const int &  i, const int &  j) {
	return grille[i][j]->typePiece();
}
