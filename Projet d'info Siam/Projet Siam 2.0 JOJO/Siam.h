#ifndef _SIAM_H_
#define _SIAM_H_

#include <vector>

#include "Elephants.h"
#include "Rhinoceros.h"
#include "Montagnes.h"
#include "Plateau.h"

using namespace std;

class Siam {

	private:
		int nbPieces;
		int nbMontagne;
		float forcePiece;
		float forcemontagne;
		std::string lettreElephant;
		std::string lettreRhinoceros;
		std::string lettreMontagne;
		std::string lettresOrientation;

		Plateau *plateau;
		vector<Elephants *> tabElephant;
		vector<Rhinoceros *> tabRhinoceros;
		vector<Montagnes *> tabMontagne;


	public:
		Siam(const int & _nbPieces, const int &  _nbMontagne, const int &  _nbLignePlateau, const int &  _nbColPlateau, const float & _forcePiece, const float & _forcemontagne, const std::string & _lettreElephant, const std::string & _lettreRhinoceros, const std::string & _lettreMontagne, const std::string & _lettresOrientation);
		
		int nombreElephants();
		int nombreRhinoceros();
		int nombreMontagnes();
		
		void jouer();
		bool validOrientation(const std::string & orientation);
		int menu(int joueur);
		void ajouterPiece(int joueur);
		void deplacerPiece(int joueur);


};

#endif