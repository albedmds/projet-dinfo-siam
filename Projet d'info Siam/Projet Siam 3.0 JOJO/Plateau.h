#ifndef _PLATEAU_H_
#define _PLATEAU_H_

#include <iostream>
#include <vector>

#include "Pieces.h"
#include "Montagnes.h"


class Plateau {

	private:
		int nbLigne;
		int nbCol;
		std::string libelleLigne;
		vector<vector<Pieces*> > grille;


	public:
		Plateau(int _nbLigne, int _nbCol);

		void ajouterPiece(Pieces* p, const int & ligne, const int & col);
		void supprimerPiece(const int & ligne, const int & col);
		void deplacerPiece(const int & lignePiece, const int & colPiece, const int & ligneDestination, const int & colDestination);
		void initialiseMontagnes(const vector<Montagnes *> & tabMontagne);
		void dessiner();

		std::string getLibelleLigne();

		bool caseValid(const int &  i, const int &  j);
		bool caseLibre(const int &  i, const int &  j);
		bool bordure(const int &  i, const int &  j);
		bool caseAdjacente(const int &  i, const int &  j, const int &  k, const int &  l);
		int typePiece(const int &  i, const int &  j);

};

#endif
