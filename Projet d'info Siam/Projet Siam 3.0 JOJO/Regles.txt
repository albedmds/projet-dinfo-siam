BUT DU JEU 
Le but de Siam est de sortir une montagne du plateau de jeu avec un de ses animaux. 
Les pions qui representent des elephants et des rhinoceros sont orientes. 
Ils peuvent se deplacer dans quatre directions. Mais s'ils poussent des adversaires, des montagnes ou les deux, 
ils doivent d'une part etre en situation majoritaire et d'autre part, orientes dans le bon sens.  
 
CARACTERISTIQUES 
Plateau de 5x5 cases, 2 joueurs, 5 elephants representes chacun par la lettre E, 5 rhinoceros par la lettre R, 3 montagnes par la lettre M. 
 
REGLES DU JEU 
Chaque joueur choisit son animal. Les joueurs joueront a tour de role. 
 
Au debut du jeu les animaux sont disposes � l'exterieur du plateau et les montagnes au centre du plateau. 
Chaque case du plateau est reperee par ses coordonnees : ligne A a E, colonne 1 a 5. 
Les elephants blancs, animaux sacres dans le royaume de SIAM commenceront a jouer. 
 Les joueurs ne pourront jouer a chaque tour de jeu qu'un seul de leur animal et ne faire qu'une des 5 actions suivantes :
 - Entrer un de ses animaux sur le plateau 
 - Se deplacer sur une case libre 
 - Changer l'orientation de son animal sans changer de case 
 - Sortir un de ses animaux disposes sur une case exterieure  
 - Se deplacer en poussant d'autres pieces disposees sur le plateau 
 
 