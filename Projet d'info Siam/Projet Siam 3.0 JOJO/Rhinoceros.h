#ifndef _RHINOCEROS_H_
#define _RHINOCEROS_H_

#include<string>

#include "PiecesJouables.h"

class Rhinoceros : public PiecesJouables {


  public:
    Rhinoceros(float _force, std::string _lettre, std::string _orientation);

	virtual int typePiece();

};

#endif