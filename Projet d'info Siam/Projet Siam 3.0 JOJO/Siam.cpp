#include "Siam.h"

using namespace std;


Siam::Siam(const int & _nbPieces, const int &  _nbMontagne, const int &  _nbLignePlateau, const int &  _nbColPlateau, const float & _forcePiece, const float & _forcemontagne, const string & _lettreElephant, const string & _lettreRhinoceros, const string & _lettreMontagne, const string & _lettresOrientation)
	: nbPieces(_nbPieces), nbMontagne(_nbMontagne), forcePiece(_forcePiece), forcemontagne(_forcemontagne), lettreElephant(_lettreElephant), lettreRhinoceros(_lettreRhinoceros), lettreMontagne(_lettreMontagne), lettresOrientation(_lettresOrientation)  {
	plateau = new Plateau(_nbLignePlateau, _nbColPlateau);
}

/*
	Retourne le nombre d elephants sur le plateau
*/
int Siam::nombreElephants() {
	return tabElephant.size();
}

/*
	Retourne le nombre de rhinoceros sur le plateau
*/
int Siam::nombreRhinoceros() {
	return tabRhinoceros.size();
}

/*
	Retourne le nombre de montagnes sur le plateau
*/
int Siam::nombreMontagnes() {
	return tabMontagne.size();
}

/*
	Verifie si la lettre utilis� pour indiquer l'orientation est valid
*/
bool Siam::validOrientation(const string & orientation) {
	bool recherche = false;
	for(int i = 0 ; i < lettresOrientation.size(); i++){
		if(lettresOrientation.substr(i,1) == orientation) {
			recherche = true;
		}
	}

	return recherche;
}

/*
	Joue une partie
*/
void Siam::jouer() {

	/* Cr�ation des Montagnes */
	for(int i = 0; i<nbMontagne; i++) {
		tabMontagne.push_back(new Montagnes(forcemontagne , lettreMontagne));
	}

	/* les Montagnes sont plac�s sur le plateau */
	plateau->initialiseMontagnes(tabMontagne);
	plateau->dessiner();

	int i = 0;

	/*
		Tant que le nombre de montagne sur le plateau est superieur ou egal au nombre de montagne initial la parti continue
	*/
	while(this->nombreMontagnes() >= nbMontagne) {
		if(i%2 == 0) {
			cout << "  "<< "______________________________________" << "  " << endl;
			cout << "| \t\t\t\t\t |" << endl;
			cout << "| \t"<< "Elephants a vous de jouer !" << "\t |" << endl;
			cout << "| "<< "______________________________________" << " |" << endl;
			cout << endl;
		}
		else {
			cout << "  "<< "______________________________________" << "  " << endl;
			cout << "| \t\t\t\t\t |" << endl;
			cout << "| \t"<< "Rhinoceros a vous de jouer !" << "\t |" << endl;
			cout << "| "<< "______________________________________" << " |" << endl;
			cout << endl;
		}

		bool validOption = false;
		while(validOption == false) {

			validOption = true;
			int option = this->menu(i);

			switch (option) {
				case 1:
					this->ajouterPiece(i);
					break;

				case 2:
					this->deplacerPiece(i);
					break;

				case 3:
					break;

				case 4:
                    this->retirerPiece(i);
					break;

				case 5:
					break;

				default:
					validOption = false;
					break;
			}

			plateau->dessiner();
		}
		i++;
	}

}

/*
	Affiche la liste des options aux joueurs
*/
int Siam::menu(int joueur) {

	int nombrePiece = 0;
	if(joueur%2 == 0) {
		nombrePiece = this->nombreElephants();
	}
	else {
		nombrePiece = this->nombreRhinoceros();
	}

	int option = 0;
	if(nombrePiece > 0) {
		cout << "Que Souhaitez vous faire ?" << endl;
		cout << "Tapez 1 pour" << "\t " << "Entrer un de vos animaux sur le plateau"   <<endl;
		cout << "Tapez 2 pour" << "\t " << "Vous deplacer sur une case libre" <<endl;
		cout << "Tapez 3 pour" << "\t " << "Changer l'orientation de votre animal" <<endl;
		cout << "Tapez 4 pour" << "\t " << "Sortir un de vos animaux du plateau" <<endl;
		cout << "Tapez 5 pour" << "\t " << "Pousser d'autres pi�ces dispos�es sur le plateau" <<endl;

		cin >> option;
	}
	else {
		option = 1;
	}

	return option;
}

/*

*/
void Siam::ajouterPiece(int joueur) {
	bool validCoordonne = false;
	int col, ligne;
	string orientation;

	do {
		cout << "Ajouter l'une de vos pieces sur le plateau" << endl;
		cout << "Entrer les coordonn�es et l'orientation (exemple 'A1b') : ";
		string coordonne = "";
		cin >> coordonne;

		if(coordonne.size()==3) {
			//col = stoi(coordonne.substr(1,1)) - 1;
			if(coordonne.substr(1,1) == "1")
				col = 0;
			if(coordonne.substr(1,1) == "2")
				col = 1;
			if(coordonne.substr(1,1) == "3")
				col = 2;
			if(coordonne.substr(1,1) == "4")
				col = 3;
			if(coordonne.substr(1,1) == "5")
				col = 4;

			ligne = plateau->getLibelleLigne().find(coordonne.substr(0,1));
			orientation = coordonne.substr(2,1);

			if(plateau->caseValid(ligne, col) && plateau->bordure(ligne, col) && plateau->caseLibre(ligne, col) && validOrientation(orientation)) {
				validCoordonne = true;
			}
		}

	} while(!validCoordonne);


	Pieces * piece = 0;
	if(joueur%2 == 0) {
		Elephants * E = new Elephants(forcePiece, lettreElephant, orientation);
		tabElephant.push_back(E);
		piece = E;
	}
	else {
		Rhinoceros * R = new Rhinoceros(forcePiece, lettreRhinoceros, orientation);
		tabRhinoceros.push_back(R);
		piece = R;
	}

	//Ajouter la piece au plateau
	plateau->ajouterPiece(piece, ligne, col);
	plateau->typePiece(ligne, col);

}


void Siam::deplacerPiece(int joueur) {
	bool validCoordonne = false;
	int colPiece, lignePiece, colDestination, ligneDestination;

	do {
		cout << "Deplacer l'une de vos pieces sur le plateau" << endl;
		cout << "Entrer les coordonnees de la piece a deplacer : ";
		string coordonnePiece = "";
		cin >> coordonnePiece;

		cout << coordonnePiece.size() <<endl;
		if(coordonnePiece.size()==2) {
			//colPiece = stoi(coordonnePiece.substr(1,1)) - 1;
			lignePiece = plateau->getLibelleLigne().find(coordonnePiece.substr(0,1));

			int typeJoueur = 0;
			if(joueur%2 == 0) {
				typeJoueur = 1;
			}
			else {
				typeJoueur = 2;
			}

			if(plateau->typePiece(lignePiece, colPiece) == typeJoueur) {

				cout << "Entrer les coordonnees de destination de la piece : ";
				string coordonneDestination = "";
				cin >> coordonneDestination;

				if(coordonneDestination.size()==2) {
					//colDestination = stoi(coordonneDestination.substr(1,1)) - 1;
					ligneDestination = plateau->getLibelleLigne().find(coordonneDestination.substr(0,1));

					//
					if(plateau->caseValid(ligneDestination, colDestination) && plateau->caseLibre(ligneDestination, colDestination) && plateau->caseAdjacente(lignePiece, colPiece, ligneDestination, colDestination)) {
						validCoordonne = true;
					}
				}
			}
		}

	}while(!validCoordonne);

	//Deplacer la piece au plateau
	plateau->deplacerPiece(lignePiece, colPiece, ligneDestination, colDestination);



}


void Siam::retirerPiece(int joueur)
{
    bool validCoordonne = false;
	int col, ligne;

	while(!validCoordonne)
    {	cout << "Retirer l'une de vos pieces sur le plateau" << endl;
		cout << "Entrer les coordonnees : ";
		string coordonne = "";
		cin >> coordonne;

		if(coordonne.size()==2) {
			//col = stoi(coordonne.substr(1,1)) - 1;
			if(coordonne.substr(1,1) == "1")
				col = 0;
			if(coordonne.substr(1,1) == "2")
				col = 1;
			if(coordonne.substr(1,1) == "3")
				col = 2;
			if(coordonne.substr(1,1) == "4")
				col = 3;
			if(coordonne.substr(1,1) == "5")
				col = 4;

			ligne = plateau->getLibelleLigne().find(coordonne.substr(0,1));


			if(plateau->caseValid(ligne, col) && plateau->bordure(ligne, col))
                {


                    if(joueur%2 == 0)
                    {
                        if(plateau->typePiece(ligne, col)==1)      //si joueur elephants ok
                            {
                                plateau->supprimerPiece(ligne,col);
                                validCoordonne = true;
                            }
                    }
                    if(joueur%2 != 0)
                     {
                        if(plateau->typePiece(ligne, col)==2)      //si joueur rhino ok
                            {
                                plateau->supprimerPiece(ligne,col);
                                validCoordonne = true;
                            }
                     }
                }
		}

    }
}





