#ifndef _ELEPHANTS_H_
#define _ELEPHANTS_H_

#include<string>

#include "PiecesJouables.h"


class Elephants : public PiecesJouables {


  public:
    Elephants(float _force, std::string _lettre, std::string _orientation);

	virtual int typePiece();


};

#endif

