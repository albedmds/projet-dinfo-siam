#include "Montagnes.h"

using namespace std;

Montagnes::Montagnes(float _force, string _lettre): Pieces(_force, _lettre) {
}

Montagnes::Montagnes(const Montagnes & montagne): Pieces(montagne.force, montagne.lettre) {
}

/*
	Retourne le symbole des montagnes a afficher sur le plateau
*/
string Montagnes::symbole() {
	return lettre;
}

int Montagnes::typePiece() {
	return 3;
}