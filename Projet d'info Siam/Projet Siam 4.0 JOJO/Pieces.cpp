#include "Pieces.h"

using namespace std;

Pieces::Pieces(float _force, string _lettre): force(_force), lettre(_lettre) {
}

Pieces::Pieces(const Pieces & piece): force(piece.force), lettre(piece.lettre) {
}

float Pieces::getForce() const {
	return force;
}