#ifndef _PIECESJOUABLES_H_
#define _PIECESJOUABLES_H_

#include <string>

#include "Pieces.h"

class PiecesJouables : public Pieces {

	protected:
		std::string orientation;


	public:
		PiecesJouables(float _force, std::string _lettre, std::string _orientation);

		std::string getOrientation() const;
		void setOrientation(std::string _orientation);
		virtual std::string symbole();

};

#endif