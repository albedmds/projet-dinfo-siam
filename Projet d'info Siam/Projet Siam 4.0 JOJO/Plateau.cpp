#include "Plateau.h"

using namespace std;

/*
	Constructeur, initialise le plateau
*/
Plateau::Plateau(int _nbLigne, int _nbCol):nbLigne(_nbLigne), nbCol(_nbCol), libelleLigne("ABCDE"), nbElephant(0), nbRhinoceros(0), nbMontagne(0) {
	for(int i = 0; i<nbLigne; i++) {
		grille.push_back(vector<Pieces *>(nbCol, nullptr));
	}
}

/*
	Destructeur
*/
Plateau::~Plateau(){
	for(int i = 0; i<nbLigne; i++) {
		for(int j = 0; j<nbCol; j++) {
			if(grille[i][j] != 0) {
				delete grille[i][j];
				grille[i][j] = 0;
			}
		}
	}
}

/*
	Retourne le nombre d elephants sur le plateau
*/
int Plateau::nombreElephants() const {
	return nbElephant;
}

/*
	Retourne le nombre de rhinoceros sur le plateau
*/
int Plateau::nombreRhinoceros() const {
	return nbRhinoceros;
}

/*
	Retourne le nombre de montagnes sur le plateau
*/
int Plateau::nombreMontagnes() const {
	return nbMontagne;
}

/*
	Ajoute un pointeur de "Pieces" au plateau
*/
void Plateau::ajouterPiece(Pieces * p, const int & ligne, const int & col) {
	grille[ligne][col] = p;
	if(this->typePiece(ligne, col) == 1)
		nbElephant++;
	if(this->typePiece(ligne, col) == 2)
		nbRhinoceros++;
	if(this->typePiece(ligne, col) == 3)
		nbMontagne++;
}

/*
	Supprime un pointeur de "Pieces" du plateau
*/
void Plateau::supprimerPiece(const int & ligne, const int & col) {
	if(this->typePiece(ligne, col) == 1)
		nbElephant--;
	if(this->typePiece(ligne, col) == 2)
		nbRhinoceros--;
	if(this->typePiece(ligne, col) == 3)
		nbMontagne--;

	delete grille[ligne][col];
	grille[ligne][col] = 0;
}

/*
	Deplace un pointeur de "Pieces" du plateau
*/
void Plateau::deplacerPiece(const int & lignePiece, const int & colPiece, const int & ligneDestination, const int & colDestination){
	this->ajouterPiece(grille[lignePiece][colPiece], ligneDestination, colDestination);
	//this->supprimerPiece(lignePiece, colPiece);
	grille[lignePiece][colPiece] = 0;
}

void Plateau::orienterPiece(const int & lignePiece, const int & colPiece, const std::string & orientation){
	try {
        PiecesJouables * pieceJouable = dynamic_cast<PiecesJouables*>(grille[lignePiece][colPiece]);
		pieceJouable->setOrientation(orientation);
    }
    catch (const exception& e)
    {
        cerr << e.what();
    }
}

void Plateau::pousserPiece(const int & lignePiece, const int & colPiece) {
	vector<Pieces*> listePieces = this->piecesAligner(lignePiece, colPiece);

	try {
        PiecesJouables * pieceJouable = dynamic_cast<PiecesJouables*>(grille[lignePiece][colPiece]);
		string orientation = pieceJouable->getOrientation();

		if(orientation == "h") {
			for(int i = listePieces.size()-1; i>=0; i--) {
				if(this->caseValid(lignePiece-i-1, colPiece)) {
					grille[lignePiece-i-1][colPiece] = listePieces[i];
					grille[lignePiece-i][colPiece] = 0;
				}
				else
				{
					this->supprimerPiece(lignePiece-i, colPiece);
				}
			}
		}

		if(orientation == "d") {
			for(int i = listePieces.size()-1; i>=0; i--) {
				if(this->caseValid(lignePiece, colPiece+i+1)) {
					grille[lignePiece][colPiece+i+1] = listePieces[i];
					grille[lignePiece][colPiece+i] = 0;
				}
				else
					this->supprimerPiece(lignePiece, colPiece+i);
			}
		}

		if(orientation == "b") {
			for(int i = listePieces.size()-1; i>=0; i--) {
				if(this->caseValid(lignePiece+i+1, colPiece)) {
					grille[lignePiece+i+1][colPiece] = listePieces[i];
					grille[lignePiece+i][colPiece] = 0;
				}
				else
				{
					this->supprimerPiece(lignePiece+i, colPiece);
				}
			}
		}

		if(orientation == "g") {
			for(int i = listePieces.size()-1; i>=0; i--) {
				if(this->caseValid(lignePiece, colPiece-i-1)) {
					grille[lignePiece][colPiece-i-1] = listePieces[i];
					grille[lignePiece][colPiece-i] = 0;
				}
				else
				{
					this->supprimerPiece(lignePiece, colPiece-i);
				}
			}
		}
    }
    catch (const exception& e)
    {
        cerr << e.what();
    }
}

/*
	Place les montagnes a leurs positions initiales (au centre du plateau)
*/
void Plateau::initialiseMontagnes(const vector<Montagnes *> & tabMontagne) {
	int tabSize = tabMontagne.size();
	for(int i = 0; i<tabSize; i++) {
		this->ajouterPiece(tabMontagne[i], i+1, 2);
	}
}

/*
	Affiche le plateau dans la console
*/
void Plateau::dessiner() const {
	cout << endl << endl;

	for(int i = 1; i<=nbCol; i++) {
		cout << "\t" << i ;
	}
	cout << endl;
	for(int i = 1; i<=nbCol; i++) {
		cout << "\t_";
	}

	cout << endl << endl;

	for(int i = 0; i<nbLigne; i++) {
		cout << " " << libelleLigne[i]<<" |";
		for(int j = 0; j<nbCol; j++) {

			if(grille[i][j] != 0) {
				cout << "\t" << grille[i][j]->symbole();
			}
			else {
				cout << "\t" << "X";
			}
		}
		cout << endl << endl;
	}

}

/*

*/
string Plateau::getLibelleLigne() const {
	return libelleLigne;
}

/*
	Retourne true si les coordonnees sont valides
*/
bool Plateau::caseValid(const int & i, const int & j) const {
	if(i>=0 && i<nbLigne && j>=0 && j<nbCol)
        return true;
    else return false;
}

/*
	Retourne true si les coordonnees correspondent a une case vide du plateau
*/
bool Plateau::caseLibre(const int &  i, const int &  j) const {
    if(grille[i][j] == 0)
        return true;
    else return false;
}

/*
	Retourne true si les coordonnees sont au bord du plateau
*/
bool Plateau::bordure(const int & i, const int & j) const {
	if((i == 0 || i == 4 || j == 0 || j == 4))
        return true;
    else return false;
}

/*
	Retourne true si les coordonnes de la case (i,j) et adjacente a la case (k,l)
*/
bool Plateau::caseAdjacente(const int & i, const int & j, const int & k, const int & l) const {
	if((i == k && (j==l+1 || j==l-1)) || (j==l && (i==k+1 || i==k-1) ))
		return true;
    else return false;
}

/*
	Identifie le type de piece sur une case
	Retourne 1 pour les Elephants, 2 pour les Rhino, 3 pour les Montagnes
*/
int Plateau::typePiece(const int &  i, const int &  j) const {
	if(grille[i][j] != 0)
		return grille[i][j]->typePiece();
	else
		return 0;
}

/*
	Retourne un tableau de toutes les pieces align�es consecutivement dans le sens de la pousser
*/
vector<Pieces*> Plateau::piecesAligner(const int & lignePiece, const int & colPiece) const {
	vector<Pieces*> listePieces;

	try {
        PiecesJouables * pieceJouable = dynamic_cast<PiecesJouables*>(grille[lignePiece][colPiece]);
		string orientation = pieceJouable->getOrientation();

		if(orientation == "h") {
			int i = lignePiece;
			while(i>=0 && grille[i][colPiece] != 0) {
				listePieces.push_back(grille[i][colPiece]);
				i--;
			}
		}
		if(orientation == "d") {
			int j = colPiece;
			while(j<nbCol && grille[lignePiece][j] != 0) {
				listePieces.push_back(grille[lignePiece][j]);
				j++;
			}
		}
		if(orientation == "b") {
			int i = lignePiece;
			while(i<nbLigne && grille[i][colPiece] != 0) {
				listePieces.push_back(grille[i][colPiece]);
				i++;
			}
		}
		if(orientation == "g") {
			int j = colPiece;
			while(j>=0 && grille[lignePiece][j] != 0) {
				listePieces.push_back(grille[lignePiece][j]);
				j--;
			}
		}
    }
    catch (const exception& e)
    {
        cerr << e.what();
    }

	return listePieces;
}
