#ifndef _PLATEAU_H_
#define _PLATEAU_H_

#include <iostream>
#include <vector>

#include "Pieces.h"
#include "PiecesJouables.h"
#include "Montagnes.h"


class Plateau {

	private:
		int nbLigne;
		int nbCol;
		int nbElephant;
		int nbRhinoceros;
		int nbMontagne;
		std::string libelleLigne;
		vector<vector<Pieces*> > grille;


	public:
		Plateau(int _nbLigne, int _nbCol);
		~Plateau();

		int nombreElephants() const;
		int nombreRhinoceros() const;
		int nombreMontagnes() const;
		std::string getLibelleLigne() const;

		void ajouterPiece(Pieces* p, const int & ligne, const int & col);
		void supprimerPiece(const int & ligne, const int & col);
		void deplacerPiece(const int & lignePiece, const int & colPiece, const int & ligneDestination, const int & colDestination);
		void orienterPiece(const int & lignePiece, const int & colPiece, const std::string & orientation);
		void pousserPiece(const int & lignePiece, const int & colPiece);

		void initialiseMontagnes(const vector<Montagnes *> & tabMontagne);
		void dessiner() const;

		bool caseValid(const int &  i, const int &  j) const;
		bool caseLibre(const int &  i, const int &  j) const;
		bool bordure(const int &  i, const int &  j) const;
		bool caseAdjacente(const int &  i, const int &  j, const int &  k, const int &  l) const;
		int typePiece(const int &  i, const int &  j) const;
		vector<Pieces*> piecesAligner(const int &  i, const int &  j) const;

};

#endif
