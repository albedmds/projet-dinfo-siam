#ifndef ELEPHANTS_H_INCLUDED
#define ELEPHANTS_H_INCLUDED

#include "PiecesJouables.h"


class Elephants : public PiecesJouables {


  public:
    Elephants();

	/*Herit� de PiecesJouables*/
    virtual void entrer();
    virtual void seDeplacer();
    virtual void changerOrientation();
    virtual void sortir();
    virtual void pousser();

};

#endif // ELEPHANTS_H_INCLUDED
