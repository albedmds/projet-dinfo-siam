#ifndef PIECESJOUABLES_H_INCLUDED
#define PIECESJOUABLES_H_INCLUDED

#include <string>

#include "Pieces.h"

class PiecesJouables : public Pieces {

  protected:
    std::string orientation;


  public:
    std::string getOrientation();
    void setOrientation(std::string _orientation);

    virtual void entrer() = 0;
    virtual void seDeplacer() = 0;
    virtual void changerOrientation() = 0;
    virtual void sortir() = 0;
    virtual void pousser() = 0;


};

#endif // PIECESJOUABLES_H_INCLUDED
