#include "Plateau.h"

using namespace std;

/*
	Constructeur, initialise le plateau
*/
Plateau::Plateau(int _nbLigne, int _nbCol):nbLigne(_nbLigne), nbCol(_nbCol) {
	for(int i = 0; i<nbLigne; i++) {
		grille.push_back(vector<Pieces *>(nbCol, nullptr));
	}
}

/*
	Retourne le nombre d elephants sur le plateau
*/
int Plateau::nombreElephants() {

	return 0;
}

/*
	Retourne le nombre de rhinoceros sur le plateau
*/
int Plateau::nombreRhinoceros() {

	return 0;
}

/*
	Retourne le nombre de montagnes sur le plateau
*/
int Plateau::nombreMontagnes() {

	return 0;
}

/*
	Ajoute un pointeur de "Pieces" au plateau
*/
void Plateau::ajouterPiece() {
}

/*
	Supprime un pointeur de "Pieces" du plateau
*/
void Plateau::supprimerPiece() {
}

/*
	Deplace un pointeur de "Pieces" du plateau
*/
void Plateau::deplacerPiece() {
}

bool Plateau::caseLibre(vector<vector<Pieces*> > grille,int i, int j)
{
    if(grille[i][j]==nullptr)
        return true;
    else return false;
}




