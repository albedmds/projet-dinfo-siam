#ifndef PLATEAU_H_INCLUDED
#define PLATEAU_H_INCLUDED

#include <vector>

#include "Pieces.h"

using namespace std;

class Plateau {

  private:
	int nbLigne;
	int nbCol;
    vector<vector<Pieces*> > grille;


  public:
    Plateau(int _nbLigne, int _nbCol);
    int nombreElephants();
    int nombreRhinoceros();
    int nombreMontagnes();
    void ajouterPiece();
    void supprimerPiece();
    void deplacerPiece();
    bool caseLibre(vector<vector<Pieces*> > grille,int i, int j);


};

#endif // PLATEAU_H_INCLUDED
