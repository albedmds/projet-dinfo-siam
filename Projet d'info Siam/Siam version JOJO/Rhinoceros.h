#ifndef RHINOCEROS_H_INCLUDED
#define RHINOCEROS_H_INCLUDED

#include "PiecesJouables.h"

class Rhinoceros : public PiecesJouables {


  public:
    Rhinoceros();

	/*Herit� de PiecesJouables*/
    virtual void entrer();
    virtual void seDeplacer();
    virtual void changerOrientation();
    virtual void sortir();
    virtual void pousser();

};

#endif // RHINOCEROS_H_INCLUDED
