#ifndef SIAM_H_INCLUDED
#define SIAM_H_INCLUDED

#include <vector>

#include "Elephants.h"
#include "Rhinoceros.h"
#include "Montagnes.h"
#include "Plateau.h"

using namespace std;

class Siam {

  private:
	int nbPieces;
	int nbMontagne;

	Plateau *plateau;
	vector<Elephants> elephant;
    vector<Rhinoceros> rhinoceros;
    vector<Montagnes> montagne;


  public:
	Siam(int _nbPieces, int _nbMontagne, int _nbLignePlateau, int _nbColPlateau);
    void jouer();
	void menu();


};

#endif // SIAM_H_INCLUDED
